#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = "Brian Højen-Sørensen"
__contact__ = "brs@fcoo.dk"
__copyright__ = "Copyright 2014, Danish Defence"
__license__ = "GNU General Public License v3.0"
__version__ = '1.0.0'
"""
Purpose: Compute the locations of high- and low pressure centers, then store
    them in NetCDF format. Intended for visualization. The script will remove
    any extrema that are too close to another, with priority for most extreme
    systems (lows before highs).

Usage: Called from command line. (run nchighlow.py -h for further information)
    python nchighlow.py [options] <infile> <outfile>

Version: 1.0.0 Initial version

NOTE: It could be considered to read the maximum windspeed (if available)
    within the (low) pressure area (e.g. window size) to categorize or rank
    the pressure systems even more.
"""

# Standard library imports
import sys
import os
import argparse
import time as tm #  to avoid conflict with NetCDF-variable 'time'
import shutil

# External imports
import netCDF4
import numpy as np
from numpy import radians, sin, cos, arctan2, sqrt, where
from scipy.ndimage.filters import minimum_filter, maximum_filter

# Local imports from pythonpath
import ncutils

RADIUS = 6371 # Earth radius
HPRESS = 1015 # Lowest value for high pressures 
LPRESS = 1020 # highest value for low pressures

def haversine_distance(lon1, lat1, lon2, lat2):
    """Compute Haversine distance"""
    dlat = radians(lat2 - lat1) / 2.0
    dlon = radians(lon2 - lon1) / 2.0
    a = sin(dlat) * sin(dlat) + cos(radians(lat1)) * cos(radians(lat2)) * sin(dlon) * sin(dlon)
    a = where(a > 1, 1.0, a)
    d = 2.0 * RADIUS * arctan2(sqrt(a), sqrt(1 - a))
    return d

def highlow():
    # Get my own name
    scriptname = os.path.basename(__file__)

    # Get command line arguments
    parser = argparse.ArgumentParser(
        description ="""
                     Compute the locations of high- and low pressure centers
                     """)
    
    parser.add_argument(dest="infile", type=str,
                        action="store", default="",
                        help="path to source file.")
    
    parser.add_argument(dest="outfile", type=str,
                        action="store", default="",
                        help="filename of the output file.")

    parser.add_argument("-d", "--distance", metavar='distance',
                        dest="distance",
                        type=float, default=500,
                        help="minimum distance between extrema in km. Default is 500 km.")
    
    parser.add_argument("-x", "--maxext", metavar='maxnum',
                        dest="maxext",
                        type=int, default=50,
                        help="Maximum number of extrema detected. Default is 50 of each.")
    
    parser.add_argument("-r", "--resolution", metavar='resolution',
                        dest="resolution",
                        type=float, default=25.0,
                        help="Preferred Model resolution in km, default = 25 km. Can be used to control amount of extremas detected.")

    parser.add_argument("-m", "--mode", metavar='mode',
                        dest="mode",
                        type=str, default='nearest',
                        help="Mode for handling boundaries when finding extrema. Options are: reflect, constant, nearest, mirror, wrap.")

    parser.add_argument("-f", "--format", metavar='format', dest="nc_format",
                        type=str, default=None,
                        help="Output NetCDF format, options are nc3, nc3_64bit, nc4, and nc4classic. Default is same as input file.")

    parser.add_argument("-e", "--edge", metavar='edge', dest="edge", type=int,
                        default=15,
                        help="Number of edgepoints excluded in the computation. Default is 15.")

    parser.add_argument("-O", "--overwrite",
                        dest="overwrite",
                        action="store_true", default=False,
                        help="Overwrite output file if it exist (or append to input file).")
    
    args       = parser.parse_args()
    infile     = args.infile
    outfile    = args.outfile
    distance   = args.distance
    maxext     = args.maxext
    resolution = args.resolution
    mode       = args.mode
    edge       = args.edge
    nc_format  = args.nc_format
    overwrite  = args.overwrite

    # Does the source file actually exist
    if not os.path.isfile(infile):
        sys.exit(scriptname+': input file: '+infile+': no such file.')

    # Check if we are trying to overwrite any files (unless overwrite = True)
    if not overwrite:
        if os.path.isfile(outfile):
            sys.exit(scriptname+': '+outfile+': file exists. Choose different name or use -O / --overwrite')

    # Open input file
    nc = netCDF4.Dataset(infile, mode='r')

    if nc_format is None:
        nc_format = nc.data_model
    elif nc_format in ['nc3', 'nc3_64bit', 'nc4', 'nc4_classic']:
        nc_formats = {'nc4' : 'NETCDF4',
                      'nc4_classic' : 'NETCDF4_CLASSIC',
                      'nc3_64bit' : 'NETCDF3_64BIT',
                      'nc3' : 'NETCDF3_CLASSIC'}
        nc_format = nc_formats[nc_format]
    else:
        sys.exit('{}: unrecognized netCDF format {}: Must be nc3, nc3_64bit, nc4, and nc4classic'.format(scriptname, nc_format))
    
    # Create (temporary)
    outtmp = outfile + '.tmp'
    ncout = netCDF4.Dataset(outtmp, mode='w', format=nc_format)

    # Access latitudes, longitudes, time, and mslp variables
    lon  = ncutils.longitude(nc)
    lat  = ncutils.latitude(nc)
    time = ncutils.temporal(nc)
    mslpvar = ncmslp(nc)
    if mslpvar.units == 'Pa':
        cfactor = .01
    elif mslpvar.units == 'hPa':
        cfactor = 1.0
    else:
        sys.exit(scriptname+': unit should be in hPa or Pa: unit is: '+mslpvar.units)

    # Compute subsetting
    if len(lon.shape) > 1: # 2d input coordinates
        res = haversine_distance(lon[0,0], lat[0,0], lon[1,0], lat[1,0])
    else: # 1d input coordinates
        res = haversine_distance(lon[0], lat[0], lon[0], lat[1])
    ss = int(round(resolution/res))

    # Avoid edges as the filter can't seem to handle them correctly
    if len(lon.shape) > 1: # 2d input coordinates
        lons = lon[edge:-edge:ss,edge:-edge:ss]
        lats = lat[edge:-edge:ss,edge:-edge:ss]
    else: # 1d input coordinates
        lons, lats = np.meshgrid(lon[edge:-edge:ss], lat[edge:-edge:ss])

    # Estimate appropriate window value from data resolution
    resolution = haversine_distance(lons[0,0], lats[0,0], lons[1,0], lats[1,0])
    window=int(5000/resolution)

    # Create old dimensions
    for dim in nc.dimensions:
        # Do not copy highlow dimensions
        if dim not in ['ext', 'str']:
            ncutils.copy_dim(nc, ncout, dim)

    # Create old variables (but do not copy data)
    for var in nc.variables:
        # Do not copy highlow variables
        if var not in ['mslext', 'latext', 'lonext']:
            ncutils.copy_var(nc, ncout, var, copy_data=False)

    # Create global attributes
    ncout.setncatts(nc.__dict__)

    # Create new variables and dimensions
    ncout.createDimension('ext', maxext*2)
    ncout.createDimension('str', 8)
    mslext = ncout.createVariable('mslext','S1',('time','ext','str'))
    mslext.units     = 'hPa'
    mslext.long_name = 'mean sealevel pressure extrema'
    mslext.coordinates = 'latext lonext'
    fill_value = netCDF4.default_fillvals['f4']
    latext = ncout.createVariable('latext','f4',('time','ext',),
                                  fill_value=fill_value)
    latext.units = 'degrees_north'
    latext.long_name = 'mslp extrema latitudes'
    lonext = ncout.createVariable('lonext','f4',('time','ext',),
                                  fill_value=fill_value)
    lonext.units = 'degrees_east'
    lonext.long_name = 'mslp extrema longitudes'

    # Loop over time steps
    for i in range(len(time)):
        # Read data
        mslp = cfactor*mslpvar[i,edge:-edge,edge:-edge][::ss,::ss]

        # Compute extrema 
        lmin, lmax = extrema(mslp, mode=mode, window=window)
        xlows = lons[lmin]; xhighs = lons[lmax]
        ylows = lats[lmin]; yhighs = lats[lmax]
        lvals = mslp[lmin];  hvals = mslp[lmax]

        # Sort and limit to maxext
        lidx = np.argsort(lvals) # lowest first
        if len(lidx) > maxext:
            lidx = lidx[:maxext]
        lvals = lvals[lidx]
        xlows = xlows[lidx]
        ylows = ylows[lidx]

        hidx = np.argsort(hvals)[::-1] # highest first
        if len(hidx) > maxext:
            hidx = hidx[:maxext]
        hvals  = hvals[hidx]
        xhighs = xhighs[hidx]
        yhighs = yhighs[hidx]

        # Remove extrema that are too close to each other (< distance)
        # Low pressure systems are more important than high, therefore they are placed first.
        lidx = []
        if len(lvals) > 0: # low pressure extrema found
            lidx = [0]
            for j in np.arange(1, len(lvals)):
                d = haversine_distance(xlows[lidx], ylows[lidx], xlows[j], ylows[j])
                if min(d) > distance and lvals[j] <= LPRESS:
                    lidx.append(j)

        hidx = []
        if len(hvals) > 0: # high pressure extrema found
            hidx = [0]
            for j in np.arange(1, len(hvals)):
                d = haversine_distance(np.hstack((xhighs[hidx],xlows[lidx])), np.hstack((yhighs[hidx],ylows[lidx])), xhighs[j], yhighs[j])
                if min(d) > distance and hvals[j] >= HPRESS:
                    hidx.append(j)

        # Create LaTeX lines
        lines = []
        for v in lvals[lidx]:
            lines.append('{0: <8}'.format('L:'+str(int(v))))
        for v in hvals[hidx]:
            lines.append('{0: <8}'.format('H:'+str(int(v))))

        # Write to outfile
        numext = len(lines)
        mslext[i,:numext] = netCDF4.stringtochar(np.array([lines], 'S'))
        latext[i,:numext] = np.hstack((ylows[lidx],yhighs[hidx]))
        lonext[i,:numext] = np.hstack((xlows[lidx],xhighs[hidx]))

    # Append history information and close output file
    nowstr = tm.strftime('%Y-%m-%d %H:%M:%S (GM)', tm.gmtime())
    hist = nc.history + '\n' + "%s %s: High and low pressure extrema computed from %s." %(nowstr, scriptname, infile)
    del ncout.history # Old bug in netCDF4, is not needed in newer versions
    ncout.history = hist

    # Copy data (faster this way)
    for var in nc.variables:
        ncutils.copy_var(nc, ncout, var, copy_data=True)

    # Close netCDF files
    nc.close()
    ncout.close()

    # Move temporary outfile to final outfile name
    shutil.move(outtmp, outfile)
 
def extrema(mat, mode='constant', window=150):
    """
    find the indices of local extrema in the input array.
    mode : reflect, constant, nearest, mirror, wrap.
    window : number of highs and lows detected.
    """
    mn = minimum_filter(mat, size=window, mode=mode)
    mx = maximum_filter(mat, size=window, mode=mode)

    # Return the indices of the maxima, minima
    return np.nonzero(mat == mn), np.nonzero(mat == mx)

def ncmslp(nc):
    """\
    Extracts mean sea-level pressure variable from a netcdf file object.
    Returns None if mean sea-level pressure not present.
    """
    p = None
    for v in nc.variables:
        ncvar = nc.variables[v]
        if ismslp(ncvar):
            p = ncvar
    return p

def ismslp(ncvar):
    """Returns True if input is mean sea-level pressure variable."""
    unam = ['mean sea level pressure',
            'air_pressure_at_sea_level',
            'pressure',
            'air_pressure_at_mean_sea_level',
            'mean sea-level pressure']
    if hasattr(ncvar, 'long_name') and getattr(ncvar, 'long_name').lower() in unam:
        return True
    elif hasattr(ncvar, 'standard_name') and getattr(ncvar, 'standard_name').lower() in unam:
        return True
    return False

if __name__ == "__main__":
    highlow()
